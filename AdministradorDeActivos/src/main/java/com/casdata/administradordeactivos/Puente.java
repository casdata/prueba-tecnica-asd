/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.casdata.administradordeactivos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
abstract public class Puente {
    static Statement myStatement;                                               //Declaracion de la conexion.
    static int xPos = 0, yPos = 0;                                              //Posicion inicial al cambiar entre frames.
    
    static boolean isMariaDBConnectionConfigured = false;                       //bandera para indicar si la configuracion con el servidor ya se habia efectuado.
    
    static DecimalFormat myDecimalFormatter;                                           //Variable para darle formato decimal con puntuacion a un entero.
    
    //Informacion para la conexion con el servidor de la base de datos
    static String myIp = "";
    static String connectionPointDb;
    static String controladorDb;
    static String usuarioDb;
    static String claveDb;
    
    static boolean mariaDbConnection(){                                         //Establece la conexion con el servidor.
        boolean tempReturn = true;
        
        try{
            Class.forName(Puente.controladorDb);
            Connection con = DriverManager.getConnection(Puente.connectionPointDb, Puente.usuarioDb,Puente.claveDb);    //Se establece la conexion co el servidor de la base de datos.
            Puente.myStatement = con.createStatement();                                                                 //Se guarda la declaracion de la conexion en myStatement.
            tempReturn = false;
        }catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Error de excepción: no se encuentra la clase.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error 0x01: No se puede conectar al servidor.");
                                                                                          //Debido a que no se puede establecer la conexion al servidor el aplicativo se cierra.
        }
        
        return tempReturn;                                                      //Si la conexion fue exitosa retorna un false, de lo contrario retorna true.
        
    }
    
    
    static boolean reconnectWhile(boolean reconnectConnection, boolean inTry){
        while(reconnectConnection){                                 //while de reconexion
                     int mySelection = JOptionPane.showOptionDialog( null,"Error: No se puede conectar al servidor.",       //Crea mensaje para decidir si se realiza una reconexion
                                                "ERROR DE CONEXION",JOptionPane.YES_NO_CANCEL_OPTION,
                                                JOptionPane.QUESTION_MESSAGE,null,// null para icono por defecto.
                                                new Object[] { "Reconectar", "Cancelar"},"opcion 1");
                    
                        if(mySelection == 0){                                   //Si se opta por reconexion
                            reconnectConnection = mariaDbConnection();          //Si la reconexion es exitosa retorna un negativo a reconnectConnection para salir del while de reconexion. 
                        }
                    
                        else{                                                   //Se cancela la tarea que se estaba llevando acabo.
                            reconnectConnection = false;
                            inTry = false;
                        }      
                    }
        
        return inTry;
        
    }
    
    static DefaultTableModel resetTable(javax.swing.JTable resetTable){         //Limpia la tabla de datos que tenga.
        DefaultTableModel tableModel = (DefaultTableModel) resetTable.getModel();   //Se toma el modelo de la tabla resetTable para asi eliminar los datos.
        int sizeTable = tableModel.getRowCount();                               //Toma el numero de filas con datos que tiene la jtable.
        if(sizeTable > 0){                                                      //Valida que tenga filas que eliminar.
            for(int i=(sizeTable-1); i>=0; i--){
                tableModel.removeRow(i);                                        //Remueve una a una los filas que hay en la jtable.
            }
        }
        
        return tableModel;                                                      //Retorna la jTabla limpia de datos.
    }
    
    static void initDecimalFormatter(){                                        //Funcion para iniciar el DecimalFormat
        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols();
        unusualSymbols.setDecimalSeparator(',');
        unusualSymbols.setGroupingSeparator('.');
        String pattern = "###,###,###,###";
        myDecimalFormatter = new DecimalFormat(pattern, unusualSymbols);
    }
    
    
}
